package com.silvioapps.latlng;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity{
    public static final long UPDATE_DELAY = 100;
    public static final float MIN_DISTANCE = 0.01f;
    public static final long MIN_TIME = 10;
    private TextView latitudeTextView = null;
    private TextView longitudeTextView = null;
    private TextView speedTextView = null;
    private TextView distanceTextView = null;
    private TextView accuracyTextView = null;
    private LatLngAsyncTask latLngAsyncTask = null;
    private boolean out = false;
    private LocationUtils locationUtils = null;
    private Button startButton = null;
    private float accuracy = 0.0f;

    private double[] lastLatLng = null;
    private double[] latLng = null;
    private Handler handler = null;
    private float speed = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitudeTextView = (TextView)findViewById(R.id.latitudeTextView);
        longitudeTextView = (TextView)findViewById(R.id.longitudeTextView);
        speedTextView = (TextView)findViewById(R.id.speedTextView);
        distanceTextView = (TextView)findViewById(R.id.distanceTextView);
        accuracyTextView = (TextView)findViewById(R.id.accuracyTextView);
        startButton = (Button)findViewById(R.id.startButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(startButton.getText().equals("Start")) {
                    start();
                }
                else{
                    stop();
                }
            }
        });

        if (latLngAsyncTask == null) {
            latLngAsyncTask = new LatLngAsyncTask();
            latLngAsyncTask.execute();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        out = true;

        if(locationUtils != null){
            locationUtils.finish();
        }

        if(latLngAsyncTask != null){
            latLngAsyncTask.cancel(true);
            latLngAsyncTask = null;
        }
    }

    protected void start(){
        if(startButton != null) {
            startButton.setText(("Stop"));

            lastLatLng = latLng;
        }
    }

    protected void stop(){
        if(startButton != null) {
            startButton.setText(("Start"));

            if(lastLatLng != null) {
                latLng = locationUtils.getLatLng();
                double distance = LocationUtils.distVincenty(lastLatLng[0], lastLatLng[1], latLng[0], latLng[1]);

                if(distanceTextView != null) {
                    distanceTextView.setText("distance " + distance + "\n");
                }
            }
        }
    }

    private class LatLngAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute(){
            handler = new Handler();

            locationUtils = new LocationUtils(MainActivity.this, MIN_TIME, MIN_DISTANCE);
            locationUtils.startSearchingLocation();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while(!out) {
                if(locationUtils != null) {

                    lastLatLng = latLng;

                    latLng = locationUtils.getLatLng();
                    speed = locationUtils.getSpeed();
                    accuracy = locationUtils.getAccuracy();
                }

                if(latLng != null && latitudeTextView != null && longitudeTextView != null && speedTextView != null && accuracyTextView != null) {
                    if (handler != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                latitudeTextView.setText("latitude " + latLng[0]);
                                longitudeTextView.setText("longitude " + latLng[1]);
                                speedTextView.setText("speed " + speed);
                                accuracyTextView.setText("accuracy "+accuracy);

                                if(locationUtils.hasNewLocation()) {
                                    //Toast.makeText(MainActivity.this, "NOVO VALOR", Toast.LENGTH_SHORT).show();
                                    locationUtils.setHasNewLocation(false);
                                }

                                 /*Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
                                List<Address> addresses = null;

                                try {
                                    addresses = geocoder.getFromLocation(latLng[0], latLng[1], 1);
                                    if (addresses != null && addresses.size() > 0) {
                                        Log.i("TAG***", "getAddressLine " + addresses.get(0).getAddressLine(0));
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }*/

                                /*if(lastLatLng != null) {
                                    double distance = LocationUtils.distance(lastLatLng[0], lastLatLng[1], latLng[0], latLng[1],"K");

                                    if(distanceTextView != null) {
                                        distanceTextView.setText("distance " + distance + "\n");
                                    }
                                }*/
                            }
                        });
                    }
                }

                try {
                    Thread.sleep(UPDATE_DELAY);
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void voids){}
    }
}
